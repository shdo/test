<?php

echo '<h1>Импорт таблицы пользователей</h1>';

// File inspection
if ( empty( $_FILES['file']['size'] )) {
	echo "<p>Файл для импорта данных не выбран</p>";
	exit();
}		
if ( $_FILES["file"]["type"] != 'text/xml') {
	echo "<p>Формат файла для импорта не верен</p>";
	exit();
}

if (!is_dir("folder")) {
	mkdir("folder", 0700);
}

// Downloads and validation file
$uploaddir = 'folder/';
$file = basename($_FILES['file']['name']);
$uploadfile = $uploaddir . $file;
move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile);
$dom = new DOMDocument;
$dom->validateOnParse = true;
$dom->Load($uploadfile);

// Parameters and database loading
require_once 'db.class.php';
DB::Connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

// Check and create the table
$create_table = mysql_query("CREATE TABLE IF NOT EXISTS `".DB_TABLE."`(`id` int (30) AUTO_INCREMENT, `login` varchar(30) NOT NULL, `password` varchar(30) NOT NULL, `username` varchar(30) NOT NULL, `email` varchar(50) NOT NULL, PRIMARY KEY(`id`))");
if (!$create_table) {
	echo "<p>Таблица не создана <b>".mysql_error()."</b></p>";
	exit();
}

$root = $dom->documentElement;
$childs = $root->childNodes;
$count = $childs->length;
for ($i = 0; $i < $childs->length; $i++) {
	$user = $childs->item($i);
	$lp = $user->childNodes;
	$login = $lp->item(0)->nodeValue;
	$password = $lp->item(1)->nodeValue;
	if ($login == '' || $password == '') {
		$i++;
		echo "<p>Ошибка в строке $i (элементы login или password имеют пустое значение)</p>";
		exit();
	}
	$getLogin = mysql_query("SELECT `login` FROM `".DB_TABLE."` WHERE `login`='".$login."' LIMIT 1");
	while($row = mysql_fetch_array($getLogin)) {
		$log =  $row["login"];
	}
	if ($login != $log) {
		$import_table = mysql_query("INSERT INTO `".DB_TABLE."` (`login`, `password`, `username`, `email`) VALUES ('".$login."', '".$password."', '".$login."', '".$login."@example.com')");
		if (!$import_table) {
			echo "<p>Импорт не завершен <b>".mysql_error()."</b></p>";
			exit();
		}
	}
}
DB::Close();

echo "<p>Обработано записей: $count</p>";