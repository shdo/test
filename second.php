<?php 

echo '<h1>Обновление таблицы пользователей</h1>';

// Check email
if (!checkEmail($_POST['email'])) {
	echo 'Не корректный email адрес';
}	
function checkEmail($email) {
    if(preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/",$email)) {
        list($username,$domain)=explode('@',$email);
            if(!checkdnsrr($domain,'MX')) {
                return false;
            }
        return true;
    }
    return false;
}

// File inspection
if ( empty( $_FILES['file']['size'] )) {
	echo "<p>Файл для обновления данных не выбран</p>";
	exit();
}
if ( $_FILES["file"]["type"] != 'text/xml' && $_FILES["file"]["type"] != 'application/vnd.ms-excel' && $_FILES["file"]["type"] != 'text/plain')	{
	echo "<p>Формат файла обновления не верен</p>";
	exit();
}

if (!is_dir("folder")) {
	mkdir("folder", 0700);
}

// Downloads and validation file
$uploaddir = 'folder/';
$file = basename($_FILES['file']['name']);
$uploadfile = $uploaddir . $file;
move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile);

// Parameters and database loading
require_once 'db.class.php';
DB::Connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

// Update of users
if ($_FILES["file"]["type"] == 'text/xml') {
	$dom = new DOMDocument;
	$dom->validateOnParse = true;
	$dom->Load($uploadfile);
	$root = $dom->documentElement;
	$childs = $root->childNodes;
	$count = $childs->length;
	$updrow = 0;
	for ($i = 0; $i < $childs->length; $i++) {
		$user = $childs->item($i);
		$lp = $user->childNodes;
		$login = $lp->item(0)->nodeValue;
		$password = $lp->item(1)->nodeValue;
		$username = $lp->item(2)->nodeValue;
		$email = $lp->item(3)->nodeValue;
		$query = sprintf("SELECT username, email FROM ".DB_TABLE." WHERE login='".$login."'", mysql_real_escape_string($username), mysql_real_escape_string($email)); 
		$result = mysql_query($query);
		$result = mysql_fetch_assoc($result);
		if ($username != $result['username'] || $email != $result['email']) {
			$update = mysql_query ("UPDATE ".DB_TABLE." SET username='$username', email='$email' WHERE login='$login'");
			if (!$update) {
				echo "Не удалось закончить обновление таблицы - ".mysql_error();
				exit;
			} else {
				$updrow++;
			}
		}
		$login_arr[] = 	$login;
	}
} else {
	$a = array();
	$fp = fopen($uploaddir . $file, "r");
	while (!feof($fp)) {
		$a[] = fgetcsv($fp, 1024, ";");
	}
	$count = count($a);
	foreach ($a as $elm) {
		if ($elm[0] == '' || $elm[2] == '' || $elm[3] == ''){
			echo 'Ошибка при обновлении записи, файл обновления имеет пустое значение';
			exit();
		}
		$query = sprintf("SELECT username, email FROM ".DB_TABLE." WHERE login='".$elm[0]."'", mysql_real_escape_string($username), mysql_real_escape_string($email));
		$result = mysql_query($query);
		$result = mysql_fetch_assoc($result);
		if ($elm[2] != $result['username'] || $elm[3] != $result['email']) {
			$update = mysql_query ("UPDATE ".DB_TABLE." SET username='$elm[2]', email='$elm[3]' WHERE login='$elm[0]'");
			if (!$update) {
				echo "Не удалось закончить обновление таблицы - ".mysql_error();
				exit;
			} else {
				$updrow++;
			}
		}
		$login_arr[] = 	$elm[0];
	}
}	
	
// Delete of unnamed users
$sql = "SELECT login FROM ".DB_TABLE; 
$tmp = mysql_query($sql); 
while($log_row = mysql_fetch_array ($tmp))  { 
   $array[] = $log_row['login']; 
}  
function key_compare_func($key1, $key2)
{
	if ($key1 == $key2)
		return 0;
	else if ($key1 > $key2)
		return 1;
	else
		return -1;
}
$del_item = 0;
$result = array_diff_ukey($array, $login_arr, 'key_compare_func');
foreach ($result as $del){
	$sql = mysql_query ("DELETE FROM ".DB_TABLE." WHERE login='".$del."'");
	$del_item++;
}
DB::Close();

// Send a report to the email
$to  = $_POST['email']; 
$subject = "Отчет по обновлению базы пользователей"; 
$message = " 
<html> 
    <head> 
        <title>Отчет по обновлению базы пользователей</title> 
    </head> 
    <body> 
        <p>Обработано записей: $count</p>
		<p>Обновленно записей: $updrow</p>	
		<p>Удалено записей: $del_item</p>
    </body> 
</html>"; 
$headers  = "Content-type: text/html; charset=utf-8 \r\n"; 
$headers .= "From: <".$to.">\r\n";
if (mail($to, $subject, $message, $headers))
  echo "<p>Обработано записей: $count</p><p>Обновленно записей: $updrow</p><p>Удалено записей: $del_item</p>";
