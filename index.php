<?php
echo '<h1>Импорт/обновление таблицы пользователей</h1>';

// Parameters and database loading
require_once 'db.class.php';

DB::Connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
$getTable = mysql_query ("SELECT * FROM `".DB_TABLE."` LIMIT 1");
if ( !$getTable ) { 
	echo '<p>Запрашиваемая таблица отсутствует. Будет созданна новая, при первом импорте</p>';
	$style = 'disabled';
} 
DB::Close();

echo'
	<p>Импорт пользователей из файла (если таблица не созданна, либо пуста)</p>
	<form enctype="multipart/form-data" action="first.php" method="POST">
		<input name="file" type="file"/>
		<input type="submit" value="Импорт" />
	</form>
	</br></br>
';
echo '
	<p>Обновление таблицы пользователей</p>
	<form enctype="multipart/form-data" action="second.php" method="POST">
		<input name="file" type="file" '.$style.'/>
		<input type="submit" value="Обновить" '.$style.'/></br>
		<p>Email для отправки отчета</p>
		<input type="text" name="email" value="" placeholder="Введите email" '.$style.' ">
	</form>
';
?>